package org.example.calc;

interface PartialExpression {
    double getValue();
    Sign getSign();

    enum Sign {
        PLUS,
        MINUS,
        DIVIDE,
        MULTIPLY;

        public String getSign() {
            switch (this) {
                case PLUS: return "+";
                case MINUS: return "-";
                case MULTIPLY: return "*";
                case DIVIDE: return "/";
                default: throw new RuntimeException("Internal error");
            }
        }
    }

    static Sign parseSign(String at)  {
        switch (at) {
            case "+": return Sign.PLUS;
            case "-": return Sign.MINUS;
            case "*": return Sign.MULTIPLY;
            case "/": return Sign.DIVIDE;
            default: throw new RuntimeException("Internal error "+at);
        }
    }

    static boolean isSign(String at) {
        return "+-/*".contains(at);
    }

}
