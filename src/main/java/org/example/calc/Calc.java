package org.example.calc;

public class Calc {

    public double calculate(String textExpression) {
        CurrentExpression currentExpression = new CurrentExpression();

        StringUtils.parseExpression(textExpression)
                .forEach(currentExpression::processExpression);

        if (currentExpression.hasNumberLeftovers())
            currentExpression.updateSumFromCurrent();

        return currentExpression.getSum();
    }

}
