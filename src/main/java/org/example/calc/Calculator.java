package org.example.calc;

public class Calculator {
    ExpressionSplitterHelper expressionSplitterHelper;
    PartialExpressionMerger partialExpressionMerger;

    public Calculator(ExpressionSplitterHelper expressionSplitterHelper, PartialExpressionMerger partialExpressionMerger) {
        this.expressionSplitterHelper = expressionSplitterHelper;
        this.partialExpressionMerger = partialExpressionMerger;
    }

    public int add(int num1, int num2) {
        return num1 + num2;
    }

    public int calculate(int num1, String operator, int num2) throws CalculatorException {
        switch (operator) {
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            case "/":
                try {
                    return num1 / num2;
                } catch (ArithmeticException e) {
                    throw new CalculatorException("Division by zero");
                }
            default:
                throw new CalculatorException("Wrong operator");
        }
    }

    public double calculate(String expression) throws CalculatorException {
        /* Delar upp uttrycket (expression) i delar räknesätt & tal  ex "2+3" blir "+2" & "+3" */
        return expressionSplitterHelper.splitExpressions(expression)
                /* Slår samman delarna med hjälp av korrekt räknesätt en efter en */
                .reduce(partialExpressionMerger::merge)
                /* Hämtar resultet av beräkningen */
                .map(PartialExpression::getValue)
                .orElseThrow(()->new CalculatorException("Empty expression"));
    }

}
