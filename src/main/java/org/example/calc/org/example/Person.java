package org.example.calc.org.example;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Value
public class Person {
    String name;
    int age;
    List<Person> children = new ArrayList<>();

    public Person addChild(String name, int age) {
        children.add(new Person(name, age));
        return this;
    }

    public int getAgeAndChildrensAge() {
        return age + children.stream()
                .mapToInt(Person::getAge)
                .sum();
    }

}
