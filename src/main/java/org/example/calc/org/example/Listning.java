package org.example.calc.org.example;

import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Listning {

    public static void main(String[] args) {

        List<Person> persons = List.of(
                new Person("Arne",111),
                new Person("Beda",99)
                        .addChild("Pekka",10)
                        .addChild("Lisa",3)
                        ,
                new Person("Gullbritt",112)
                        .addChild("Kalle",3)
                ,
                new Person("Adam",72),
                new Person("Pelle",82)
                        .addChild("Stefan",3)
                ,
                new Person("Rudolf",88),
                new Person("Carin",87)
                        .addChild("Lena",12)
        );

        Map<Integer, List<Person>> personsByAge = persons.stream()
                .flatMap(person -> Stream.concat(Stream.of(person), person.getChildren().stream()))
                .collect(Collectors.groupingBy(Person::getAge));

        printAgeAndNames(3, List.of());

        //BiConsumer<Integer, List<Person>> print = Listning::printAgeAndNames;
        BiConsumer<Integer, List<Person>> print = Listning::printAgeAndNamesCount;

        personsByAge.forEach(print);

        //System.out.println("Listning.main "+sum);


        // Summera ålder på alla personer
        // med och/eller utan streams
    }

    static public void printAgeAndNames(Integer age, List<Person> p) {
        System.out.println(""+age+" "+p.stream().map(Person::getName).collect(Collectors.joining(",")));
    }

    static public void printAgeAndNamesCount(Integer age, List<Person> p) {
        System.out.println(""+age+" "+p.size());
    }
}

/*



        for (Iterator<Person> iterator = persons.iterator(); iterator.hasNext(); ) {
            Person next = iterator.next();
            if (next.getAge() <= 100)
                System.out.println("Listning.main "+next);
        }

        for (int i = 0; i < persons.size(); i++) {
             Person person = persons.get(i);
             System.out.println("Listning.main "+person);
        }

        for (Person next : persons) {
            if (next.getAge() <= 100)
                System.out.println("Listning.main " + next);
        }

        System.out.println("Listning.main -----------------");

        List<Person> personsBelow100 = persons.stream()
                .filter(person -> person.getAge() <= 100)
                .collect(Collectors.toList());

        personsBelow100.forEach(System.out::println);

        // List personer på olika vis + print
        // Filtrera bort äldre än 100
        // Också med streams

 */
