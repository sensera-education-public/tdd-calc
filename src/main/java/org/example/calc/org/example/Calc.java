package org.example.calc.org.example;

import lombok.Value;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Calc {

    public static int add(ValuePair valuePair) {
        return valuePair.getA() + valuePair.getB();
    }

    public static int sub(ValuePair valuePair) {
        return valuePair.getA() - valuePair.getB();
    }

    public static int mul(ValuePair valuePair) {
        return valuePair.getA() * valuePair.getB();
    }

    public static int calc(Function<ValuePair,Integer> func, ValuePair valuePair) {
        return func.apply(valuePair);
    }

    public static void main(String[] args) {
        List<ValuePair> pairs = List.of(
                ValuePair.create(10, 20),
                ValuePair.create(4, 6),
                ValuePair.create(100, 80)
        );

        calcAndPrint(pairs, Calc::add);

        System.out.println("Calc.main ---------------------------");

        calcAndPrint(pairs, Calc::sub);

        System.out.println("Calc.main ---------------------------");

        calcAndPrint(pairs, Calc::mul);
        //int sum = calc(Calc::sub, 10, 15);

        //System.out.println("Calc.main "+sum);

    }

    private static void calcAndPrint(List<ValuePair> pairs, Function<ValuePair, Integer> add) {
        pairs.stream()
                .map(add)
                .forEach(sum -> System.out.println("Calc.main " + sum));
    }

    @Value(staticConstructor = "create")
    static class ValuePair {
        int a;
        int b;
    }
}
