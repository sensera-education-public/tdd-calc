package org.example.calc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PartialExpressionMerger {

    public PartialExpression merge(PartialExpression partialExpression1, PartialExpression partialExpression2) {
        switch (partialExpression2.getSign()) {
            case PLUS: return createPartialExpression(partialExpression1.getValue() + partialExpression2.getValue(), partialExpression1.getSign());
            case MINUS: return createPartialExpression(partialExpression1.getValue() - partialExpression2.getValue(), partialExpression1.getSign());
            case MULTIPLY: return createPartialExpression(partialExpression1.getValue() * partialExpression2.getValue(), partialExpression1.getSign());
            case DIVIDE: return createPartialExpression(partialExpression1.getValue() / partialExpression2.getValue(), partialExpression1.getSign());
        }
        return partialExpression1;
    }

    private PartialExpression createPartialExpression(double value, PartialExpression.Sign sign) {
        return new PartialExpression() {
            @Override
            public double getValue() {
                return value;
            }

            @Override
            public Sign getSign() {
                return sign;
            }
        };
    }


    public Stream<PartialExpression> mergePrio(Stream<PartialExpression> partialExpressions) {
        Iterator<PartialExpression> iterator = partialExpressions.collect(Collectors.toList()).iterator();
        if (!iterator.hasNext())
            return Stream.empty();
        PartialExpression start = iterator.next();
        List<PartialExpression> ret = new ArrayList<>();
        ret.add(start);
        while (iterator.hasNext()) {
            PartialExpression at = iterator.next();
            if (at.getSign().equals(PartialExpression.Sign.MULTIPLY) || at.getSign().equals(PartialExpression.Sign.DIVIDE)) {
                start = merge(start, at);
            } else {
                ret.add(at);
                start = at;
            }
        }
        return ret.stream();
    }
}
