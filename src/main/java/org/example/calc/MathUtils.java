package org.example.calc;

public interface MathUtils {

    static boolean isNumber(String at) {
        return "0123456789".contains(at);
    }

    static boolean isSign(String v) {
        return "+-*/".contains(v);
    }

    static double calc(double sum, String sign, double value) {
        switch (sign) {
            case "+": return sum + value;
            case "-": return sum - value;
            case "*": return sum * value;
            case "/": return sum / value;
            default: throw new RuntimeException("Internal error!");
        }
    }

}
