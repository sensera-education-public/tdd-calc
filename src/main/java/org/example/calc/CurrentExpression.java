package org.example.calc;

import static org.example.calc.MathUtils.isNumber;
import static org.example.calc.MathUtils.isSign;

class CurrentExpression {
    double sum = 0;
    String currentNumber = "";
    String currentSign = "+";

    public void updateSumFromCurrent() {
        sum = MathUtils.calc(sum, currentSign, Double.parseDouble(currentNumber));
        currentNumber = "";
    }

    public void updateSign(String sign) {
        currentSign = sign;
    }

    public void addToCurrentNumber(String v) {
        currentNumber += v;
    }

    public boolean hasNumberLeftovers() {
        return !currentNumber.equals("");
    }

    public void processExpression(String partialExpression) {
        if (isNumber(partialExpression)) {
            addToCurrentNumber(partialExpression);
        } else if (isSign(partialExpression)) {
            updateSumFromCurrent();
            updateSign(partialExpression);
        }
    }

    public double getSum() {
        return sum;
    }
}
