package org.example.calc;

import java.util.ArrayList;
import java.util.List;

public interface StringUtils {

    static List<String> parseExpression(String expression) {
        List<String> res = new ArrayList<>();
        for (int i = 0; i < expression.length(); i++) {
            res.add(expression.substring(i, i + 1));
        }
        return res;
    }
}
