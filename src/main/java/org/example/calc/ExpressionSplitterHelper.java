package org.example.calc;

import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class ExpressionSplitterHelper {

    PartialExpressionMerger partialExpressionMerger = new PartialExpressionMerger();

    public Stream<PartialExpression> splitExpressions(String expression) {
        /* Skapar en "tracker" object som innehåller alla delade beräkningar */
        PartialExpressionTracker partialExpressionTracker = new PartialExpressionTracker();
        CurrentTracker currentTracker = new CurrentTracker(partialExpressionTracker);

        /* Delar strängen i invididuella "characters" och processar dem en efter en med hjälp av "tracker" */
        eachCharacterInExpression(expression, at ->
                processEachCharacter(at,
                        currentTracker.getPartialExpressionTracker(),
                        currentTracker::setPartialExpressionTracker));

        if (partialExpressionTracker.notEmptyBuffer()) {
            partialExpressionTracker.createPartialExpression();
        }

        return partialExpressionTracker.partialExpressions();
    }

    private static void processEachCharacter(String at, PartialExpressionTracker partialExpressionTracker, Consumer<PartialExpressionTracker> updateTacker) {
        // "2*(2+5)"
        // "2*(2+5*(7+1))" "2*(2+5*(2+3))"
        if (at.equals("(")) {
            updateTacker.accept(partialExpressionTracker.beginParenthesis());
        }
        if (at.equals(")")) {
            updateTacker.accept(partialExpressionTracker.endParenthesis());
        }
        if (MathUtils.isNumber(at)) {
            partialExpressionTracker.addToBuffer(at);
            return;
        }
        if (partialExpressionTracker.notEmptyBuffer()) {
            partialExpressionTracker.createPartialExpression();
        }
        if (PartialExpression.isSign(at)) {
            partialExpressionTracker.setSign(PartialExpression.parseSign(at));
        }
    }

    private static void eachCharacterInExpression(String expression, Consumer<String> consumer) {
        for (int i = 0; i < expression.length(); i++) {
            consumer.accept(expression.substring(i, i + 1));
        }
    }

    @Value
    private static class ValuePartialExpression implements PartialExpression {
        double value;
        Sign sign;
    }

    private class PartialExpressionTracker {
        PartialExpressionTracker parent;
        List<PartialExpression> partialExpressions = new ArrayList<>();
        PartialExpression.Sign sign;
        String buffer;

        public PartialExpressionTracker() {
            this.parent = null;
            this.sign = PartialExpression.Sign.PLUS;
            this.buffer = null;
        }

        public PartialExpressionTracker(PartialExpressionTracker parent) {
            this.parent = parent;
            this.sign = PartialExpression.Sign.MULTIPLY;
            this.buffer = null;
        }

        public void addToBuffer(String at) {
            buffer = buffer != null ? (buffer + at) : at;
        }

        public boolean notEmptyBuffer() {
            return buffer != null;
        }

        public void createPartialExpression() {
            partialExpressions.add(new ValuePartialExpression(Double.parseDouble(buffer), sign));
            buffer = null;
        }

        public void setSign(PartialExpression.Sign parseSign) {
             sign = parseSign;
        }

        public Stream<PartialExpression> partialExpressions() { return partialExpressions.stream(); }

        public PartialExpressionTracker beginParenthesis() {
            return new PartialExpressionTracker(this);
        }

        public PartialExpressionTracker endParenthesis() {
            if (notEmptyBuffer())
                createPartialExpression();
            partialExpressions()
                    .reduce(partialExpressionMerger::merge)
                    .ifPresent(partialExpression -> parent.add(partialExpression));
            return parent;
        }

        private void add(PartialExpression partialExpression) {
            partialExpressions.add(partialExpression);
        }
    }

    @Data
    private class CurrentTracker {
        PartialExpressionTracker partialExpressionTracker;

        public CurrentTracker(PartialExpressionTracker partialExpressionTracker) {
            this.partialExpressionTracker = partialExpressionTracker;
        }
    }
}
