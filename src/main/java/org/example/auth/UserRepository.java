package org.example.auth;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserRepository {
    Map<String,User> users;

    public UserRepository(Stream<User> users) {
        this.users = users.collect(Collectors.toMap(User::getUsername, user -> user));
    }

    public User getUserByUserName(String username) {
        return users.get(username);
    }
}
