package org.example.auth;

import java.util.UUID;

public class TokenGenerator {
    public String createToken(User user) {
        return UUID.randomUUID().toString();
    }
}
