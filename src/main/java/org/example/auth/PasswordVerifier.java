package org.example.auth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class PasswordVerifier {

    public boolean verifyPassword(byte[] passwordHash, String password) {
        return Arrays.equals(passwordHash, getDigestedPassword(password));
    }

    public byte[] getDigestedPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            return md.digest();
       } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Internal error",e);
        }
    }
}
