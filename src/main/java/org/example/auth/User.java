package org.example.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

@Data
@AllArgsConstructor
public class User {
    String username;
    byte[] passwordHash;

}
