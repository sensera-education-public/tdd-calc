package org.example.auth;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AuthService {
    private UserRepository userRepository;
    private PasswordVerifier passwordVerifyer;
    private TokenGenerator tokenGenerator;

    public String login(String username, String password) {
        User user = userRepository.getUserByUserName(username);
        if (user==null)
            return null;
        if (!passwordVerifyer.verifyPassword(user.getPasswordHash(), password))
            return null;
        return tokenGenerator.createToken(user);
    }
}
