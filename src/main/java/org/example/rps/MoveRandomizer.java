package org.example.rps;

import java.util.Random;

public class MoveRandomizer {
    Random random = new Random();

    public RPSMove randomMove() {
        switch (random.nextInt(2)) {
            case 0:
                return RPSMove.SCISSORS;
            case 1:
                return RPSMove.PAPER;
            case 2:
                return RPSMove.ROCK;
            default:
                throw new RuntimeException("Not!");
        }
    }
}
