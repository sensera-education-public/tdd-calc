package org.example.rps;

public class RPSGameLogic {

    public RPSResult calculateWinnerOfMove(RPSMove move1, RPSMove move2) {
        if (move1.equals(move2))
            return RPSResult.DRAW;
        switch (move1) {
            case SCISSORS: return move2.equals(RPSMove.PAPER) ? RPSResult.WIN : RPSResult.LOSE;
            case PAPER: return move2.equals(RPSMove.ROCK) ? RPSResult.WIN : RPSResult.LOSE;
            case ROCK: return move2.equals(RPSMove.SCISSORS) ? RPSResult.WIN : RPSResult.LOSE;
            default:
                throw new RuntimeException("Unknown move "+move1);
        }
    }
}
