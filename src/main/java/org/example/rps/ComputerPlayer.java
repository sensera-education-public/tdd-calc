package org.example.rps;

public class ComputerPlayer implements Player {
    private MoveRandomizer moveRandomizer;

    public ComputerPlayer(MoveRandomizer moveRandomizer) {
        this.moveRandomizer = moveRandomizer;
    }

    @Override
    public RPSMove makeMove() {
        return moveRandomizer.randomMove();
    }
}
