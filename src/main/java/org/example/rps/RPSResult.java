package org.example.rps;

public enum RPSResult {
    WIN,
    LOSE,
    DRAW
}
