package org.example.rps;

import java.util.Scanner;

public class GameInput {
    Scanner scanner = new Scanner(System.in);

    public String getInput() {
        return scanner.nextLine();
    }
}
