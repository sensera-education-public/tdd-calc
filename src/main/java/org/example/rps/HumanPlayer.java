package org.example.rps;

public class HumanPlayer implements Player {
    private GameInput gameInput;

    public HumanPlayer(GameInput gameInput) {
        this.gameInput = gameInput;
    }

    @Override
    public RPSMove makeMove() {
        return RPSMove.valueOf(gameInput.getInput());
    }
}
