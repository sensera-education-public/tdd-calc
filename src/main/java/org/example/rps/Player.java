package org.example.rps;

public interface Player {
    RPSMove makeMove();
}
