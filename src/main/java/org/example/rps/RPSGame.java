package org.example.rps;

public class RPSGame {
    private final RPSGameLogic rpsGameLogic;
    private final Player player1;
    private final Player player2;
    private int gameRounds;

    public RPSGame(RPSGameLogic rpsGameLogic, Player player1, Player player2, int gameRounds) {
        this.rpsGameLogic = rpsGameLogic;
        this.player1 = player1;
        this.player2 = player2;
        this.gameRounds = gameRounds;
    }

    public RPSResult playRound() {
        return rpsGameLogic.calculateWinnerOfMove(
                player1.makeMove(),
                player2.makeMove());
    }

    public RPSGameSession play() {
        return new RPSGameSession(this);
    }

    public int getGameRounds() {
        return gameRounds;
    }
}
