package org.example.rps;

public class GameOutput {
    public void displayPlayComputerVsPlayerIntro() {
        System.out.println("Play vs computer");
    }

    public void askHumanPlayerToMakeMove() {
        System.out.println("Make your move (ROCK,PAPER,SCISSORS)");
    }

    public void displayGameResult(RPSResult result) {
        if (result.equals(RPSResult.WIN))
            System.out.println("You win");
        if (result.equals(RPSResult.LOSE))
            System.out.println("You lose");
        if (result.equals(RPSResult.DRAW))
            System.out.println("Its a draw!");
    }
}
