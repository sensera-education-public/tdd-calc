package org.example.rps;

public enum RPSMove {
    ROCK,
    PAPER,
    SCISSORS,
}
