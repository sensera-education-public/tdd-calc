package org.example.rps;

public class ActualGame {
    private final GameInput gameInput;
    private final GameOutput gameOutput;
    private final MoveRandomizer moveRandomizer;

    public ActualGame(GameInput gameInput, GameOutput gameOutput, MoveRandomizer moveRandomizer) {
        this.gameInput = gameInput;
        this.gameOutput = gameOutput;
        this.moveRandomizer = moveRandomizer;
    }

    public void playComputerVsPlayer() {
        gameOutput.displayPlayComputerVsPlayerIntro();
        Player player1 = new HumanPlayer(gameInput);
        Player player2 = new ComputerPlayer(moveRandomizer);
        RPSGame rpsGame = new RPSGame(new RPSGameLogic(), player1, player2, 3);
        RPSGameSession play = rpsGame.play();
        while (!play.isCompleted()) {
            gameOutput.askHumanPlayerToMakeMove();
            play.playOneRound();
        }
        gameOutput.displayGameResult(play.getResult());
    }

    public static void main(String[] args) {
        ActualGame actualGame = new ActualGame(new GameInput(), new GameOutput(), new MoveRandomizer());

        actualGame.playComputerVsPlayer();
    }
}
