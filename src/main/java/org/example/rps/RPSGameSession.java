package org.example.rps;

public class RPSGameSession {
    RPSGame rpsGame;
    private int gameRound;
    private int gamePlayer1Score;
    private int gamePlayer2Score;

    public RPSGameSession(RPSGame rpsGame) {
        this.rpsGame = rpsGame;
    }

    public RPSResult getResult() {
        if (!isCompleted())
            return null;
        if (gamePlayer1Score > gamePlayer2Score)
            return RPSResult.WIN;
        if (gamePlayer1Score < gamePlayer2Score)
            return RPSResult.LOSE;
        return RPSResult.DRAW;
    }

    public boolean isCompleted() {
        return gameRound == rpsGame.getGameRounds();
    }

    public void playOneRound() {
        gameRound++;
        RPSResult result = rpsGame.playRound();
        if (result.equals(RPSResult.WIN))
            gamePlayer1Score++;
        if (result.equals(RPSResult.LOSE))
            gamePlayer2Score++;
    }

    public int getGameRoundNumber() {
        return gameRound;
    }

    public int getGameScorePlayer1() {
        return gamePlayer1Score;
    }

    public int getGameScorePlayer2() {
        return gamePlayer2Score;
    }
}
