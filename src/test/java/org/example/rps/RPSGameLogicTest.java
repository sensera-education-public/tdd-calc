package org.example.rps;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RPSGameLogicTest {

    RPSGameLogic rPSGameLogic = new RPSGameLogic();

    @Test
    void test_calculate_winner_of_moves_success() {
        assertEquals(RPSResult.LOSE, rPSGameLogic.calculateWinnerOfMove(RPSMove.PAPER, RPSMove.SCISSORS));
        assertEquals(RPSResult.DRAW, rPSGameLogic.calculateWinnerOfMove(RPSMove.SCISSORS, RPSMove.SCISSORS));
        assertEquals(RPSResult.WIN, rPSGameLogic.calculateWinnerOfMove(RPSMove.SCISSORS, RPSMove.PAPER));
    }
}
