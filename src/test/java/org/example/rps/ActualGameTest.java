package org.example.rps;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class ActualGameTest {

    @Test
    void test_play_human_vs_computer_success() {
        GameInput gameInput = mock(GameInput.class);
        GameOutput gameOutput = mock(GameOutput.class);
        MoveRandomizer moveRandomizer = mock(MoveRandomizer.class);
        ActualGame actualGame = new ActualGame(gameInput, gameOutput, moveRandomizer);
        when(moveRandomizer.randomMove()).thenReturn(RPSMove.ROCK);
        when(gameInput.getInput()).thenReturn("PAPER");

        actualGame.playComputerVsPlayer();

        verify(gameInput, times(3)).getInput();
        verify(gameOutput, times(1)).displayGameResult(any(RPSResult.class));
        verify(gameOutput, times(1)).displayPlayComputerVsPlayerIntro();
        verify(gameOutput, times(3)).askHumanPlayerToMakeMove();
    }
}
