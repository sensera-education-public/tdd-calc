package org.example.rps;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RPSGameTest {

    @Test
    void test_play_game_round_success() {
        RPSGameLogic rpsGameLogic = new RPSGameLogic();
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        RPSGame rpsGame = new RPSGame(rpsGameLogic, player1, player2, 3);
        when(player1.makeMove()).thenReturn(RPSMove.ROCK);
        when(player2.makeMove()).thenReturn(RPSMove.SCISSORS);

        RPSResult result = rpsGame.playRound();

        assertEquals(RPSResult.WIN, result);
    }

    /*@Test
    void test_play_game_success() {
        RPSGameLogic rpsGameLogic = new RPSGameLogic();
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        RPSGame rpsGame = new RPSGame(rpsGameLogic, player1, player2, 3);
        when(player1.makeMove()).thenReturn(RPSMove.ROCK, RPSMove.ROCK, RPSMove.ROCK);
        when(player2.makeMove()).thenReturn(RPSMove.SCISSORS, RPSMove.SCISSORS, RPSMove.SCISSORS);

        RPSResult result = rpsGame.play();

        assertEquals(RPSResult.WIN, result);
    }*/
}
