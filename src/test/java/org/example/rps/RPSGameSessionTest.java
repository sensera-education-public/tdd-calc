package org.example.rps;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RPSGameSessionTest {

    @Test
    void test_create_game_session_success() {
        RPSGame rpsGame = mock(RPSGame.class);
        when(rpsGame.getGameRounds()).thenReturn(3);

        RPSGameSession gameSession = new RPSGameSession(rpsGame);

        assertNull(gameSession.getResult());
        assertFalse(gameSession.isCompleted());
        assertEquals(0, gameSession.getGameRoundNumber());
        assertEquals(0, gameSession.getGameScorePlayer1());
        assertEquals(0, gameSession.getGameScorePlayer2());
    }

    @Test
    void test_one_game_round_success() {
        RPSGame rpsGame = mock(RPSGame.class);
        RPSGameSession gameSession = new RPSGameSession(rpsGame);
        when(rpsGame.playRound()).thenReturn(RPSResult.WIN);

        gameSession.playOneRound();

        assertNull(gameSession.getResult());
        assertFalse(gameSession.isCompleted());
        assertEquals(1, gameSession.getGameRoundNumber());
        assertEquals(1, gameSession.getGameScorePlayer1());
        assertEquals(0, gameSession.getGameScorePlayer2());
    }

    @Test
    void test_completed_game_success() {
        RPSGame rpsGame = mock(RPSGame.class);
        RPSGameSession gameSession = new RPSGameSession(rpsGame);
        when(rpsGame.getGameRounds()).thenReturn(3);
        when(rpsGame.playRound()).thenReturn(RPSResult.WIN);
        gameSession.playOneRound();
        gameSession.playOneRound();

        gameSession.playOneRound();

        assertEquals(RPSResult.WIN, gameSession.getResult());
        assertTrue(gameSession.isCompleted());
        assertEquals(3, gameSession.getGameRoundNumber());
        assertEquals(3, gameSession.getGameScorePlayer1());
        assertEquals(0, gameSession.getGameScorePlayer2());
    }
}
