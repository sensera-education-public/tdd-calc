package org.example.calc;

import org.example.calc.ExpressionSplitterHelper;
import org.example.calc.PartialExpression;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ExpressionSplitterHelperTest {

    ExpressionSplitterHelper expressionSplitterHelper = new ExpressionSplitterHelper();

    @ParameterizedTest
    @MethodSource("provideExpressions")
    void test_split_expression_success(List<Double> expectedValues, List<Double> expectedSigns, String expression) {
        // When
        List<PartialExpression> expressions = expressionSplitterHelper.splitExpressions(expression)
                .collect(Collectors.toList());

        // Then
        assertEquals(expectedValues, expressions.stream().map(PartialExpression::getValue).collect(Collectors.toList()));
        assertEquals(expectedSigns, expressions.stream().map(PartialExpression::getSign).collect(Collectors.toList()));
    }

    private static Stream<Arguments> provideExpressions() {
        return Stream.of(
                Arguments.of(List.of(2D,3D), List.of(PartialExpression.Sign.PLUS, PartialExpression.Sign.PLUS), "2+3"),
                Arguments.of(List.of(20D,30D), List.of(PartialExpression.Sign.PLUS, PartialExpression.Sign.PLUS), "20+30"),
                Arguments.of(List.of(20D,30D), List.of(PartialExpression.Sign.PLUS, PartialExpression.Sign.MINUS), "20-30"),
                Arguments.of(List.of(20D,30D), List.of(PartialExpression.Sign.MINUS, PartialExpression.Sign.MULTIPLY), "-20*30"),
                Arguments.of(List.of(20D,30D, 5D), List.of(PartialExpression.Sign.MINUS, PartialExpression.Sign.MULTIPLY, PartialExpression.Sign.MINUS), "-20*30-5"),

                Arguments.of(List.of(2D, 7D), List.of(PartialExpression.Sign.PLUS, PartialExpression.Sign.MULTIPLY), "2*(2+5)"),
                Arguments.of(List.of(2D, 7D, 3D, 7D), List.of(PartialExpression.Sign.PLUS, PartialExpression.Sign.MULTIPLY, PartialExpression.Sign.MINUS, PartialExpression.Sign.MULTIPLY), "2*(2+5)-3*(3+4)"),
                Arguments.of(List.of(2D, 27D), List.of(PartialExpression.Sign.PLUS, PartialExpression.Sign.MULTIPLY), "2*(2+5*(2+3))")
        );
    }
}
