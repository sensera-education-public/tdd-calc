package org.example.calc;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MathUtilsTest {

    @Test
    void test_calc_success() {
        assertEquals(10, MathUtils.calc(5,"+",5));
        assertEquals(0, MathUtils.calc(5,"-",5));
        // ... fler kombinationer
    }

    @Test
    void test_calc_fail_because_wrong_sign() {
        RuntimeException runtimeException = assertThrows(RuntimeException.class, () -> MathUtils.calc(1, "", 2));

        assertEquals("Internal error!", runtimeException.getMessage());
    }
}
