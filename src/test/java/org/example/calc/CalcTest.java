package org.example.calc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalcTest {

    Calc calc;

    @BeforeEach
    void setUp() {
        calc = new Calc();
    }

    @ParameterizedTest
    @MethodSource("provideExpressions")
    void test_standard_calculation_success(double expectedSum, String expression) {
        double sum = calc.calculate(expression);

        assertEquals(expectedSum, sum);
    }

    private static Stream<Arguments> provideExpressions() {
        return Stream.of(
                Arguments.of(5, "2+3"),
                Arguments.of(6, "3+3"),
                Arguments.of(0, "3-3"),
                Arguments.of(27, "30-3"),
                Arguments.of(37, "30-3+10"),
                Arguments.of(0, "30-3*10")
        );
    }

}
