package org.example.calc;

import org.example.calc.PartialExpression;
import org.example.calc.PartialExpressionMerger;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PartialExpressionMergerTest {
    PartialExpressionMerger partialExpressionMerger = new PartialExpressionMerger();

    @Test
    void test_calculate_sum_of_two_values_success() {
        // Given
        PartialExpression partialExpression1 = mock(PartialExpression.class);
        PartialExpression partialExpression2 = mock(PartialExpression.class);
        when(partialExpression1.getValue()).thenReturn(2D);
        when(partialExpression2.getValue()).thenReturn(3D);
        when(partialExpression1.getSign()).thenReturn(PartialExpression.Sign.PLUS);
        when(partialExpression2.getSign()).thenReturn(PartialExpression.Sign.PLUS);

        // When
        PartialExpression merge = partialExpressionMerger.merge(partialExpression1, partialExpression2);

        // Then
        assertEquals(5, merge.getValue());
        assertEquals(PartialExpression.Sign.PLUS, merge.getSign());
    }

    @Test
    void test_calculate_sum_expression_success() {
        PartialExpression partialExpression1 = mock(PartialExpression.class);
        PartialExpression partialExpression2 = mock(PartialExpression.class);
        PartialExpression partialExpression3 = mock(PartialExpression.class);
        when(partialExpression1.getValue()).thenReturn(2D);
        when(partialExpression2.getValue()).thenReturn(5D);
        when(partialExpression3.getValue()).thenReturn(5D);
        when(partialExpression1.getSign()).thenReturn(PartialExpression.Sign.PLUS);
        when(partialExpression2.getSign()).thenReturn(PartialExpression.Sign.PLUS);
        when(partialExpression2.getSign()).thenReturn(PartialExpression.Sign.MULTIPLY);

        List<PartialExpression> merges = partialExpressionMerger.mergePrio(Stream.of(partialExpression1, partialExpression2, partialExpression3))
                .collect(Collectors.toList());

        assertEquals(List.of(2D, 25D), merges.stream().map(PartialExpression::getValue).collect(Collectors.toList()));
        assertEquals(List.of(PartialExpression.Sign.PLUS, PartialExpression.Sign.MULTIPLY), merges.stream().map(PartialExpression::getSign).collect(Collectors.toList()));
    }
}
