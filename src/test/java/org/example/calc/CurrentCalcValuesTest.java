package org.example.calc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrentCalcValuesTest {

    CurrentExpression currentCalcValues;

    @BeforeEach
    void setUp() {
        currentCalcValues = new CurrentExpression();
    }

    @Test
    void test_process_expression_with_number_input_success() {
        // When
        currentCalcValues.processExpression("1");

        // Then
        assertEquals("1", currentCalcValues.currentNumber);
        assertEquals("+", currentCalcValues.currentSign);
        assertEquals(0D, currentCalcValues.getSum());
    }

    @Test
    void test_process_expression_with_sign_input_success() {
        // Given
        currentCalcValues.currentNumber = "8";

        // When
        currentCalcValues.processExpression("-");

        // Then
        assertEquals("", currentCalcValues.currentNumber);
        assertEquals("-", currentCalcValues.currentSign);
        assertEquals(8D, currentCalcValues.getSum());
    }

    @Test
    void test_process_expression_with_sign_input_and_sum_success() {
        // Given
        currentCalcValues.sum = 3;
        currentCalcValues.currentNumber = "8";

        // When
        currentCalcValues.processExpression("-");

        // Then
        assertEquals("", currentCalcValues.currentNumber);
        assertEquals("-", currentCalcValues.currentSign);
        assertEquals(11D, currentCalcValues.getSum());
    }

    @Test
    void test_process_expression_with_priority_sign_input_and_sum_success() {
        // "30-3*10"
        // Given
        currentCalcValues.sum = 3;
        currentCalcValues.currentNumber = "8";

        // When
        currentCalcValues.processExpression("*");

        // Then
        assertEquals("", currentCalcValues.currentNumber);
        assertEquals("*", currentCalcValues.currentSign);
        assertEquals(11D, currentCalcValues.getSum());
    }

    @Test
    void test_update_sum_from_current_success() {
        // Given
        currentCalcValues.currentNumber = "8";

        // When
        currentCalcValues.updateSumFromCurrent();

        // Then
        assertEquals("", currentCalcValues.currentNumber);
        assertEquals("+", currentCalcValues.currentSign);
        assertEquals(8D, currentCalcValues.getSum());
    }

    @Test
    void fail() {
        // When
        NumberFormatException numberFormatException = assertThrows(NumberFormatException.class, () -> currentCalcValues.updateSumFromCurrent());

        // Then
        assertEquals("empty String", numberFormatException.getMessage());
    }
}
