package org.example.calc;

import org.example.calc.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CalculatorTest {

    Calculator calculator;
    ExpressionSplitterHelper expressionSplitterHelper;
    PartialExpressionMerger partialExpressionMerger;

    @BeforeEach
    void setUp() {
        expressionSplitterHelper = mock(ExpressionSplitterHelper.class);
        partialExpressionMerger = mock(PartialExpressionMerger.class);
        calculator = new Calculator(expressionSplitterHelper, partialExpressionMerger);
    }

    @Test
    void test_add_two_numbers() {
        assertEquals(5, calculator.add(2, 3));
        assertEquals(10, calculator.add(4, 6));
        assertEquals(15, calculator.add(4, 11));
        assertEquals(20, calculator.add(4, 16));
    }

    @Test
    void test_add_different_types_success() throws CalculatorException {
        assertEquals(5, calculator.calculate(2, "+", 3));
        assertEquals(10, calculator.calculate(4, "+", 6));
        assertEquals(4, calculator.calculate(10, "-", 6));
        assertEquals(2, calculator.calculate(8, "-", 6));
        assertEquals(25, calculator.calculate(5, "*", 5));
        assertEquals(10, calculator.calculate(2, "*", 5));
        assertEquals(1, calculator.calculate(5, "/", 5));
        assertEquals(5, calculator.calculate(10, "/", 2));
    }

    @Test
    void test_calc_failed_because_division_zero() {
        CalculatorException calculatorException = assertThrows(CalculatorException.class, () -> calculator.calculate(5, "/", 0));

        assertEquals("Division by zero", calculatorException.getMessage());
    }

    @Test
    void test_calc_failed_because_wrong_operator() {
        CalculatorException calculatorException = assertThrows(CalculatorException.class, () -> calculator.calculate(5, "h", 1));

        assertEquals("Wrong operator", calculatorException.getMessage());
    }

    @ParameterizedTest
    @MethodSource("provideExpressions")
    void test_calculate_expression_success(double expected, String expression) throws CalculatorException {
        // Given
        PartialExpression partialExpression = mock(PartialExpression.class);
        when(expressionSplitterHelper.splitExpressions(eq(expression))).thenReturn(Stream.of(partialExpression));
        when(partialExpression.getValue()).thenReturn(expected);

        // When
        double result = calculator.calculate(expression);

        // Then
        assertEquals(expected, result);
    }

    private static Stream<Arguments> provideExpressions() {
        return Stream.of(
                Arguments.of(5.0, "2+3")
        );
    }

    @Test
    void test_calculate_merge_expression_success() throws CalculatorException {
        // Given
        PartialExpression partialExpression1 = mock(PartialExpression.class);
        PartialExpression partialExpression2 = mock(PartialExpression.class);
        PartialExpression partialExpressionResult = mock(PartialExpression.class);
        when(expressionSplitterHelper.splitExpressions(eq("expression"))).thenReturn(Stream.of(partialExpression1,partialExpression2));
        when(partialExpressionMerger.merge(eq(partialExpression1),eq(partialExpression2))).thenReturn(partialExpressionResult);
        when(partialExpressionResult.getValue()).thenReturn(6D);

        // When
        double result = calculator.calculate("expression");

        // Then
        assertEquals(6D, result);
    }

    @Test
    void test_calculate_expression_fail_because_expression_empty() {
        // Given
        when(expressionSplitterHelper.splitExpressions(eq(""))).thenReturn(Stream.empty());

        // When
        CalculatorException calculatorException = assertThrows(CalculatorException.class, () -> calculator.calculate(""));

        // Then
        assertEquals("Empty expression", calculatorException.getMessage());
    }
}
