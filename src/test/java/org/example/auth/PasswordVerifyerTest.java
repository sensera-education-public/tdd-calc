package org.example.auth;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordVerifyerTest {

    @Test
    void name() {
        PasswordVerifier passwordVerifyer = new PasswordVerifier();

        assertTrue(passwordVerifyer.verifyPassword(passwordVerifyer.getDigestedPassword("pw"),"pw"));
    }

    @Test
    void name2() {
        PasswordVerifier passwordVerifyer = new PasswordVerifier();

        assertTrue(passwordVerifyer.verifyPassword(passwordVerifyer.getDigestedPassword("password"),"password"));
    }

    @Test
    void name3() {
        PasswordVerifier passwordVerifyer = new PasswordVerifier();

        assertFalse(passwordVerifyer.verifyPassword(passwordVerifyer.getDigestedPassword("pw"),"password"));
    }
}
