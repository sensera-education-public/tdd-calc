package org.example.auth;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class AuthServiceTest {

    private UserRepository userReporitory;
    private PasswordVerifier passwodVerifyer;
    private TokenGenerator tokenGenerator;
    private AuthService auth;

    @BeforeEach
    void setUp() {
        userReporitory = mock(UserRepository.class);
        passwodVerifyer = mock(PasswordVerifier.class);
        tokenGenerator = mock(TokenGenerator.class);
        auth = new AuthService(userReporitory, passwodVerifyer, tokenGenerator);
    }

    @Test
    void test_login_success() {
        // Given
        User user = new User("","".getBytes());
        String generatedToken = UUID.randomUUID().toString();

        when(userReporitory.getUserByUserName(eq("arne"))).thenReturn(user);
        when(passwodVerifyer.verifyPassword(any(), eq("password"))).thenReturn(true);
        when(tokenGenerator.createToken(eq(user))).thenReturn(generatedToken);

        // When
        String token = auth.login("arne","password");

        // Then
        assertEquals(generatedToken, token);
    }

    @Test
    void test_login_fail_wrong_username() {
        // Given
        when(userReporitory.getUserByUserName(any())).thenReturn(null);

        // When
        String token = auth.login("arne","password");

        // Then
        assertNull(token);
        verifyNoInteractions(passwodVerifyer);
        verifyNoInteractions(tokenGenerator);
    }

    @Test
    void test_login_fail_wrong_password() {
        // Given
        User user = new User("","".getBytes());

        when(userReporitory.getUserByUserName(eq("arne"))).thenReturn(user);
        when(passwodVerifyer.verifyPassword(any(), any())).thenReturn(false);

        // When
        String token = auth.login("arne","password");

        // Then
        assertNull(token);
        verifyNoInteractions(tokenGenerator);
    }
}
