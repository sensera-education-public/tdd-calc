package org.example.auth;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class AuthServiceNoMocksTest {

    private UserRepository userReporitory;
    private PasswordVerifier passwodVerifyer;
    private TokenGenerator tokenGenerator;
    private AuthService auth;

    @BeforeEach
    void setUp() {
        passwodVerifyer = new PasswordVerifier();
        User user = new User("arne",passwodVerifyer.getDigestedPassword("password"));

        userReporitory = new UserRepository(Stream.of(user));
        tokenGenerator = new TokenGenerator();
        auth = new AuthService(userReporitory, passwodVerifyer, tokenGenerator);
    }

    @Test
    void test_login_success() {
        // When
        String token = auth.login("arne","password");

        // Then
        assertNotNull(token);
    }

    @Test
    void test_login_fail_wrong_username() {
        // When
        String token = auth.login("gullbritt","password");

        // Then
        assertNull(token);
    }

    @Test
    void test_login_fail_wrong_password() {
        // When
        String token = auth.login("arne","wrong");

        // Then
        assertNull(token);
    }
}
